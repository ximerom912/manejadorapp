const AccessControl = require('accesscontrol');
const control = new AccessControl();

exports.roles = (function(){
    control.grant("normal").readAny("backlongs").readAny("burndown").readAny("files").readAny("members").readAny("productBacklogs").readAny("proyectOwners").readAny("releaseBcklogs")
    .readAny("scrumMasters").readAny("sprintBacklog").readAny("userHistory").readAny("users");
    control.grant("admin").readAny("backlongs").readAny("burndown").readAny("files").readAny("members").readAny("productBacklogs").readAny("proyectOwners").readAny("releaseBcklogs")
    .readAny("scrumMasters").readAny("sprintBacklog").readAny("userHistory").readAny("users").createAny("backlongs").createAny("burndown").createAny("files").createAny("members").createAny("productBacklogs").createAny("proyectOwners").createAny("releaseBcklogs")
    .createAny("scrumMasters").createAny("sprintBacklog").createAny("userHistory").createAny("users").updateAny("backlongs").updateAny("burndown").updateAny("files").updateAny("members").updateAny("productBacklogs").updateAny("proyectOwners").updateAny("releaseBcklogs")
    .updateAny("scrumMasters").updateAny("sprintBacklog").updateAny("userHistory").updateAny("users").deleteAny("backlongs").deleteAny("burndown").updateAny("files").updateAny("members").updateAny("productBacklogs").updateAny("proyectOwners").updateAny("releaseBcklogs")
    .deleteAny("scrumMasters").deleteAny("sprintBacklog").deleteAny("userHistory").deleteAny("users");
})
