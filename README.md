# Proyecto reto I: Desarrollar un manejador de proyectos

Proyecto reto I: Este proyecto consistira en aplicación web que implemente un manejador de proyectos de software basado en la metodología scrum.


![Diagrama de clases](https://gitlab.com/ximerom912/manejadorapp/-/blob/main/diagramaProyecto.jpg)
![Diagrama de flujo UML](https://gitlab.com/ximerom912/manejadorapp/-/blob/main/diagramaflujo.jpg)


### Prerequisitos

Se tiene que contar con alguna version de Docker, así como descargar el repositorio para tener los archivos en el ordenador y posteriormente desplegarlo en la terminal.

### Instalación

Para descargar las dependencias de npm:

```
npm install
```

## Ejecución las pruebas

Para correr el servidor, se utilizan los siguientes comandos en terminal:
```
npm start
```
o
```
DEBUG=manejadorapp:* npm start
```

## Construido con

* [Visual Studio Code](https://code.visualstudio.com/) - Editor de texto
* [Express js](https://expressjs.com/es/) - Framework para JavaScript

## Lenguajes

* javaScript

## Contribuciones

Sin contribuciones exteriores

## Versiones

Version 1.0.1

## Autores

* **Melissa Garcia Mendoza** 
* **Jesus Martín Loeara Esparza** 
* **Ximena Romero Chavez** 

## Licencia

Este proyecto no tiene licencia.

## Acknowledgments