const supertest = require('supertest');
const app = require('../app');
var key = ""
//sentencia
describe('Probar el sistema de autentificacion', ()=>{
    //casos de prueba 50%
    it('Deberia de obtener un login con curp y contraseña correcto', (done)=>{
        supertest(app).post('/login')
        .send({'curp':'loej1234', 'password':'martinloco'})
        .expect(200)
        .end(function(err, res){
            key = res.body.obj;
            done()
        })
    })
})

describe('Probar lista de productBacklogs', ()=>{
    it('Deberia de obtener la lista de productBacklogs', ()=>{
        supertest(app)
        .get('/productBacklogs/')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    })
    it('Deberia de no obtener la lista de productBacklogss', ()=>{
        supertest(app)
        .get('/productBacklogs/')
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})


describe('Probar obtener productBacklogs por index', ()=>{
    it('Deberia de obtener el productBacklogs con index', ()=>{
        supertest(app)
        .get('/productBacklogs/61a9a625eda8bfad05050920')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no obtener el productBacklogs con index', ()=>{
        supertest(app)
        .get('/productBacklogs/61a9a625eda8bfad0505092')
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar crear productBacklogs', ()=>{
    it('Deberia de crear un productBacklogs nuevo', ()=>{
        supertest(app)
        .post('/productBacklogs/')
        .send({
            nombre: "backlog 1",
            tiempo: 5,
            historias: ['61a993aeeda8bfad0505090e']
        })
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no crear un productBacklogs nuevo', ()=>{
        supertest(app)
        .post('/productBacklogs/')
        .send({
            nombre: "backlog 1235",
            tiempo: 10,
            historias: ['61a993aeeda8bfad0505090e']
        })
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar crear remplazar datos de productBacklogs', ()=>{
    it('Deberia de modificar productBacklogs con id', ()=>{
        supertest(app)
        .put('/productBacklogs/61a9a625eda8bfad05050920')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombre: "backlog 10",
            tiempo: 4,
            historias: ['61a993aeeda8bfad0505090e']
        })
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no modificar productBacklogs con id', ()=>{
        supertest(app)
        .put('/productBacklogs/61a9a625eda8bfad0505092')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombre: "backlog 144",
            tiempo: 20,
            historias: ['61a993aeeda8bfad0505090e']
        })
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar editar datos de productBacklogs', ()=>{
    it('Deberia de modificar productBacklogs con id', ()=>{
        supertest(app)
        .patch('/productBacklogs/61a9a625eda8bfad05050920')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombre: "backlog 252",
            tiempo: 252,
            historias: ['61a993aeeda8bfad0505090e']
        })
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no modificar productBacklogs con id', ()=>{
        supertest(app)
        .patch('/productBacklogs/61a9a625eda8bfad05050920')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombre: "backlog 99",
            tiempo: 777,
            historias: ['61a993aeeda8bfad0505090e']
        })        
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar eliminar datos de productBacklogs', ()=>{
    it('Deberia de eliminar productBacklogs con id', ()=>{
        supertest(app)
        .delete('/productBacklogs/61a9a625eda8bfad05050920')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no eliminar productBacklogs con id', ()=>{
        supertest(app)
        .delete('/productBacklogs/61a9a625eda8bfad0505092')
        .set('Authorization', `Bearer ${key}`)
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})
