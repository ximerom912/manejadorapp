const supertest = require('supertest');
const app = require('../app');
var key = ""
//sentencia
describe('Probar el sistema de autentificacion', ()=>{
    //casos de prueba 50%
    it('Deberia de obtener un login con curp y contraseña correcto', (done)=>{
        supertest(app).post('/login')
        .send({'curp':'loej1234', 'password':'martinloco'})
        .expect(200)
        .end(function(err, res){
            key = res.body.obj;
            done()
        })
    })
})

describe('Probar lista de proyectOwner', ()=>{
    it('Deberia de obtener la lista de proyectOwner', ()=>{
        supertest(app)
        .get('/proyectOwner/')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    })
    it('Deberia de no obtener la lista de proyectOwners', ()=>{
        supertest(app)
        .get('/proyectOwner/')
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})


describe('Probar obtener proyectOwner por index', ()=>{
    it('Deberia de obtener el proyectOwner con index', ()=>{
        supertest(app)
        .get('/proyectOwner/61a9a4d7eda8bfad0505091d')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no obtener el proyectOwner con index', ()=>{
        supertest(app)
        .get('/proyectOwner/61a9a4d7eda8bfad0505091')
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar crear proyectOwner', ()=>{
    it('Deberia de crear un proyectOwner nuevo', ()=>{
        supertest(app)
        .post('/proyectOwner/')
        .send({rol: 'owner'})
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no crear un proyectOwner nuevo', ()=>{
        supertest(app)
        .post('/proyectOwner/')
        .send({rol: 'developer'})
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar crear remplazar datos de proyectOwner', ()=>{
    it('Deberia de modificar proyectOwner con id', ()=>{
        supertest(app)
        .put('/proyectOwner/61a9a4d7eda8bfad0505091d')
        .set('Authorization', `Bearer ${key}`)
        .send({rol: 'developer'})
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no modificar proyectOwner con id', ()=>{
        supertest(app)
        .put('/proyectOwner/61a9a4d7eda8bfad0505091')
        .set('Authorization', `Bearer ${key}`)
        .send({rol: 'scrumaster'})
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar editar datos de proyectOwner', ()=>{
    it('Deberia de modificar proyectOwner con id', ()=>{
        supertest(app)
        .patch('/proyectOwner/61a9a4d7eda8bfad0505091d')
        .set('Authorization', `Bearer ${key}`)
        .send({rol: 'scrumaster'})
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no modificar proyectOwner con id', ()=>{
        supertest(app)
        .patch('/proyectOwner/61a9a4d7eda8bfad0505091')
        .set('Authorization', `Bearer ${key}`)
        .send({rol: 'developer'})        
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar eliminar datos de proyectOwner', ()=>{
    it('Deberia de eliminar proyectOwner con id', ()=>{
        supertest(app)
        .delete('/proyectOwner/61a9a4d7eda8bfad0505091d')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no eliminar proyectOwner con id', ()=>{
        supertest(app)
        .delete('/proyectOwner/61a9a4d7eda8bfad0505091')
        .set('Authorization', `Bearer ${key}`)
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})
