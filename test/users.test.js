const supertest = require('supertest');
const app = require('../app');
var key = ""
//sentencia
describe('Probar el sistema de autentificacion', ()=>{
    //casos de prueba 50%
    it('Deberia de obtener un login con curp y contraseña correcto', (done)=>{
        supertest(app).post('/login')
        .send({'curp':'loej1234', 'password':'martinloco'})
        .expect(200)
        .end(function(err, res){
            key = res.body.obj;
            done()
        })
    })
})

describe('Probar lista de usuarios', ()=>{
    it('Deberia de obtener la lista de usuarios', ()=>{
        supertest(app)
        .get('/users/')
        .set('Authorization', `Bearer ${key}`)
        .end(function(err,res){
            if (err) {
                done(err)
            }else{
                expect(res.statusCode).toEqual(200)
                done()

            }
        })
    })
    it('Deberia de no obtener la lista de usuarios', ()=>{
        supertest(app)
        .get('/users/')
        .set('Authorization', `Bearer ${key}`)
        .end(function(err,res){
            if (err) {
                done(err)
            }else{
                expect(res.statusCode).toEqual(500)
                done()

            }
        })
    })
})


describe('Probar obtener usuario por index', ()=>{
    it('Deberia de obtener el usuario con index', ()=>{
        supertest(app)
        .get('/users/61a9719a28033bd1099e20bf')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no obtener el usuario con index', ()=>{
        supertest(app)
        .get('/users/61a9719a28033bd1099e20b')
        .set('Authorization', `Bearer ${key}`)
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar crear usuario', ()=>{
    it('Deberia de crear un usuario nuevo', ()=>{
        supertest(app)
        .post('/users/')
        .send({
            curp: 'spartan1234', 
            password: 'holamundo',
            fecha: '12-17-20',
            rfc: 'loeje1254',
            domicilio: 'molino inca prim 23',
            nombre: 'Martin Loera'
        })
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no crear un usuario nuevo', ()=>{
        supertest(app)
        .post('/users/')
        .send({
            curp: 'curli14', 
            password: 'holamundo',
            fecha: '12-17-20',
            rfc: 'loeje1254',
            domicilio: 'molino inca prim 23',
            nombre: 'Mar Loera'
        })
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar crear remplazar datos de usuario', ()=>{
    it('Deberia de modificar usuario con id', ()=>{
        supertest(app)
        .put('/users/61a9719a28033bd1099e20bf')
        .set('Authorization', `Bearer ${key}`)
        .send({
            curp: 'marvo1234', 
            password: 'holamundo',
            fecha: '12-17-20',
            rfc: 'loeje1254',
            domicilio: 'molino inca prim 23',
            nombre: 'Martinsan Loera'
        })
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no modificar usuario con id', ()=>{
        supertest(app)
        .put('/users/61a9719a28033bd1099e20bf')
        .set('Authorization', `Bearer ${key}`)
        .send({
            curp: 'marvo1234', 
            password: 'holamundo',
            fecha: '12-17-20',
            rfc: 'loeje1254',
            domicilio: 'molino inca prim 23',
            nombre: 'Martinchan Loera'
        })
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar editar datos de usuario', ()=>{
    it('Deberia de modificar usuario con id', ()=>{
        supertest(app)
        .patch('/users/61a9719a28033bd1099e20bf')
        .set('Authorization', `Bearer ${key}`)
        .send({
            curp: 'marvo1234', 
            password: 'holamundo',
            fecha: '12-17-20',
            rfc: 'loeje1254',
            domicilio: 'molino inca prim 23',
            nombre: 'Martintilin Loera'
        })
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no modificar usuario con id', ()=>{
        supertest(app)
        .patch('/users/61a9719a28033bd1099e20b')
        .set('Authorization', `Bearer ${key}`)
        .send({
            curp: 'marvo1234', 
            password: 'holamundo',
            fecha: '12-17-20',
            rfc: 'loeje1254',
            domicilio: 'molino inca prim 23',
            nombre: 'Martin Loera'
        })
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar eliminar datos de usuario', ()=>{
    it('Deberia de eliminar usuario con id', ()=>{
        supertest(app)
        .delete('/users/61a9719a28033bd1099e20bf')
        .set('Authorization', `Bearer ${key}`)
        .send({id: '61a9719a28033bd1099e20bf'})
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no eliminar usuario con id', ()=>{
        supertest(app)
        .delete('/users/61a9719a28033bd1099e20b')
        .set('Authorization', `Bearer ${key}`)
        .send({id: '61a9719a28033bd1099e20b'})
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})
