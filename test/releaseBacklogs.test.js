const supertest = require('supertest');
const app = require('../app');
var key = ""
//sentencia
describe('Probar el sistema de autentificacion', ()=>{
    //casos de prueba 50%
    it('Deberia de obtener un login con curp y contraseña correcto', (done)=>{
        supertest(app).post('/login')
        .send({'curp':'loej1234', 'password':'martinloco'})
        .expect(200)
        .end(function(err, res){
            key = res.body.obj;
            done()
        })
    })
})

describe('Probar lista de releaseBacklogs', ()=>{
    it('Deberia de obtener la lista de releaseBacklogs', ()=>{
        supertest(app)
        .get('/releaseBacklogs/')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    })
    it('Deberia de no obtener la lista de releaseBacklogss', ()=>{
        supertest(app)
        .get('/releaseBacklogs/')
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})


describe('Probar obtener releaseBacklogs por index', ()=>{
    it('Deberia de obtener el releaseBacklogs con index', ()=>{
        supertest(app)
        .get('/releaseBacklogs/61a9a2a8eda8bfad0505091a')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no obtener el releaseBacklogs con index', ()=>{
        supertest(app)
        .get('/releaseBacklogs/61a9a2a8eda8bfad0505091a')
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar crear releaseBacklogs', ()=>{
    it('Deberia de crear un releaseBacklogs nuevo', ()=>{
        supertest(app)
        .post('/releaseBacklogs/')
        .send({
            nombre: "release 1",
            tiempo: 10,
            historias: ['61a993aeeda8bfad0505090e']
        })
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no crear un releaseBacklogs nuevo', ()=>{
        supertest(app)
        .post('/releaseBacklogs/')
        .send({
            nombre: "release 2",
            tiempo: 25,
            historias: ['61a993aeeda8bfad0505090e']
        })
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar crear remplazar datos de releaseBacklogs', ()=>{
    it('Deberia de modificar releaseBacklogs con id', ()=>{
        supertest(app)
        .put('/releaseBacklogs/61a9a2a8eda8bfad0505091a')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombre: "release 20",
            tiempo: 25,
            historias: ['61a993aeeda8bfad0505090e']
        })
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no modificar releaseBacklogs con id', ()=>{
        supertest(app)
        .put('/releaseBacklogs/61a9a2a8eda8bfad0505091')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombre: "release 0",
            tiempo: 25,
            historias: ['61a993aeeda8bfad0505090e']
        })
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar editar datos de releaseBacklogs', ()=>{
    it('Deberia de modificar releaseBacklogs con id', ()=>{
        supertest(app)
        .patch('/releaseBacklogs/61a9a2a8eda8bfad0505091a')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombre: "release 10",
            tiempo: 1,
            historias: ['61a993aeeda8bfad0505090e']
        })
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no modificar releaseBacklogs con id', ()=>{
        supertest(app)
        .patch('/releaseBacklogs/61a9a2a8eda8bfad0505091')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombre: "release 120",
            tiempo: 0,
            historias: ['61a993aeeda8bfad0505090e']
        })        
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar eliminar datos de releaseBacklogs', ()=>{
    it('Deberia de eliminar releaseBacklogs con id', ()=>{
        supertest(app)
        .delete('/releaseBacklogs/61a9a2a8eda8bfad0505091a')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no eliminar releaseBacklogs con id', ()=>{
        supertest(app)
        .delete('/releaseBacklogs/61a9a2a8eda8bfad0505091')
        .set('Authorization', `Bearer ${key}`)
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})
