const supertest = require('supertest');
const app = require('../app');
var key = ""
//sentencia
describe('Probar el sistema de autentificacion', ()=>{
    //casos de prueba 50%
    it('Deberia de obtener un login con curp y contraseña correcto', (done)=>{
        supertest(app).post('/login')
        .send({'curp':'loej1234', 'password':'martinloco'})
        .expect(200)
        .end(function(err, res){
            key = res.body.obj;
            done()
        })
    })
})

describe('Probar lista de historias de historias de usuario', ()=>{
    it('Deberia de obtener la lista de historias de historias de usuario', ()=>{
        supertest(app)
        .get('/userHistories/')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    })
    it('Deberia de no obtener la lista de historias de historias de usuario', ()=>{
        supertest(app)
        supertest(app)
        .get('/userHistories/')
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})


describe('Probar obtener historias de usuario por index', ()=>{
    it('Deberia de obtener el historias de usuario con index', ()=>{
        supertest(app)
        .get('/userHistories/61a993aeeda8bfad0505090e')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no obtener el historias de usuario con index', ()=>{
        supertest(app)
        .get('/userHistories/61a993aeeda8bfad0505090')
        .set('Authorization', `Bearer ${key}`)
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar crear historias de usuario', ()=>{
    it('Deberia de crear un historias de usuario nuevo', ()=>{
        supertest(app)
        .post('/userHistories/')
        .send({
            nombre: "Historia 1",
            rol: "developer",
            funcionalidad: "Crear un compas musical",
            beneficio: "Crear musica",
            prioridad: "Alta",
            tamano: 3,
            resultado: "Poder hacer compas musical",
            terminado: false,
            puntos: 2,
            aceptado: true
        })
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no crear un historias de usuario nuevo', ()=>{
        supertest(app)
        .post('/userHistories/')
        .send({
            nombre: "Historia 2",
            rol: "developer",
            funcionalidad: "Crear un compas musicales",
            beneficio: "Crear musica",
            prioridad: "Alta",
            tamano: 3,
            resultado: "Poder hacer compas musical",
            terminado: false,
            puntos: 2,
            aceptado: false
        })
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar crear remplazar datos de historias de usuario', ()=>{
    it('Deberia de modificar historias de usuario con id', ()=>{
        supertest(app)
        .put('/userHistories/61a993aeeda8bfad0505090')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombre: "Historia 1",
            rol: "developer",
            funcionalidad: "Crear un cohete",
            beneficio: "Crear cohete en css",
            prioridad: "baja",
            tamano: 3,
            resultado: "Poder ver un cohete",
            terminado: true,
            puntos: 1,
            aceptado: false
        })
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no modificar historias de usuario con id', ()=>{
        supertest(app)
        .put('/userHistories/61a993aeeda8bfad0505090')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombre: "Historia 1",
            rol: "developer",
            funcionalidad: "Crear un compas musical para niños",
            beneficio: "Crear musica",
            prioridad: "Alta",
            tamano: 3,
            resultado: "Poder hacer compas musical",
            terminado: false,
            puntos: 2,
            aceptado: true
        })
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})


describe('Probar editar datos de historias de usuario', ()=>{
    it('Deberia de modificar historias de usuario con id', ()=>{
        supertest(app)
        .patch('/userHistories/61a993aeeda8bfad050509')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombre: "Historia 2",
            rol: "developer",
            funcionalidad: "Crear un compas musical niños de 30 años",
            beneficio: "Crear musica",
            prioridad: "Alta",
            tamano: 3,
            resultado: "Poder hacer compas musical",
            terminado: false,
            puntos: 2,
            aceptado: false
        })
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no modificar historias de usuario con id', ()=>{
        supertest(app)
        .patch('/userHistories/61a993aeeda8bfad05050')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombre: "Historia 3",
            rol: "developer",
            funcionalidad: "Crear un compas musical",
            beneficio: "Crear musica",
            prioridad: "Alta",
            tamano: 3,
            resultado: "Poder hacer compas musical",
            terminado: false,
            puntos: 2,
            aceptado: true
        })
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar eliminar datos de historias de usuario', ()=>{
    it('Deberia de eliminar historias de usuario con id', ()=>{
        supertest(app)
        .delete('/userHistories/61a993aeeda8bfad050509')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no eliminar historias de usuario con id', ()=>{
        supertest(app)
        .delete('/userHistories/61a993aeeda8bfad05050')
        .set('Authorization', `Bearer ${key}`)
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})
