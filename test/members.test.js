const supertest = require('supertest');
const app = require('../app');
var key = ""
//sentencia
describe('Probar el sistema de autentificacion', ()=>{
    //casos de prueba 50%
    it('Deberia de obtener un login con curp y contraseña correcto', (done)=>{
        supertest(app).post('/login')
        .send({'curp':'loej1234', 'password':'martinloco'})
        .expect(200)
        .end(function(err, res){
            key = res.body.obj;
            done()
        })
    })
})

describe('Probar lista de members', ()=>{
    it('Deberia de obtener la lista de members', ()=>{
        supertest(app)
        .get('/members/')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    })
    it('Deberia de no obtener la lista de memberss', ()=>{
        supertest(app)
        .get('/members/')
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})


describe('Probar obtener members por index', ()=>{
    it('Deberia de obtener el members con index', ()=>{
        supertest(app)
        .get('/members/61a9a7bfeda8bfad05050922')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no obtener el members con index', ()=>{
        supertest(app)
        .get('/members/61a9a7bfeda8bfad05050922')
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar crear members', ()=>{
    it('Deberia de crear un members nuevo', ()=>{
        supertest(app)
        .post('/members/')
        .send({
            listadeHabilidades: ['js', 'node'],
            rol: "developer"
        })
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no crear un members nuevo', ()=>{
        supertest(app)
        .post('/members/')
        .send({
            listadeHabilidades: ['js', 'node', 'quimica'],
            rol: "QBP"
        })
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar crear remplazar datos de members', ()=>{
    it('Deberia de modificar members con id', ()=>{
        supertest(app)
        .put('/members/61a9a7bfeda8bfad05050922')
        .set('Authorization', `Bearer ${key}`)
        .send({
            listadeHabilidades: ['criticar'],
            rol: "owner"
        })
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no modificar members con id', ()=>{
        supertest(app)
        .put('/members/61a9a7bfeda8bfad0505092')
        .set('Authorization', `Bearer ${key}`)
        .send({
            listadeHabilidades: ['js', 'node', 'bailar'],
            rol: "developer bailarin"
        })
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar editar datos de members', ()=>{
    it('Deberia de modificar members con id', ()=>{
        supertest(app)
        .patch('/members/61a9a7bfeda8bfad05050922')
        .set('Authorization', `Bearer ${key}`)
        .send({
            listadeHabilidades: ['js', 'node', 'python'],
            rol: "developer"
        })
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no modificar members con id', ()=>{
        supertest(app)
        .patch('/members/61a9a7bfeda8bfad0505092')
        .set('Authorization', `Bearer ${key}`)
        .send({
            listadeHabilidades: ['js', 'node'],
            rol: "developerito"
        })        
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar eliminar datos de members', ()=>{
    it('Deberia de eliminar members con id', ()=>{
        supertest(app)
        .delete('/members/61a9a7bfeda8bfad05050922')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no eliminar members con id', ()=>{
        supertest(app)
        .delete('/members/61a9a7bfeda8bfad0505092')
        .set('Authorization', `Bearer ${key}`)
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})
