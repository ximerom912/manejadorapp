const supertest = require('supertest');
const app = require('../app');
var key = ""
//sentencia
describe('Probar el sistema de autentificacion', ()=>{
    //casos de prueba 50%
    it('Deberia de obtener un login con curp y contraseña correcto', (done)=>{
        supertest(app).post('/login')
        .send({'curp':'loej1234', 'password':'martinloco'})
        .expect(200)
        .end(function(err, res){
            key = res.body.obj;
            done()
        })
    })
    it('Deberia de obtener un login con curp y contraseña incorrecto', (done)=>{
        supertest(app).post('/login')
        .send({'curp':'loej123', 'password':'martinloco'})
        .expect(500)
        .end(function(err, res){
            done()
        })
    })
})

describe('Probar que la ruta raiz este funcionado', ()=>{
    it('Deberia acceder a la ruta localhost:3000/ correctamente', (done)=>{
        supertest(app).get('/')
        .expect(200)
        .end(function(err, res){
            done()
        })
    })
})