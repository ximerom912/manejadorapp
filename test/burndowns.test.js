const supertest = require('supertest');
const app = require('../app');
var key = ""
//sentencia
describe('Probar el sistema de autentificacion', ()=>{
    //casos de prueba 50%
    it('Deberia de obtener un login con curp y contraseña correcto', (done)=>{
        supertest(app).post('/login')
        .send({'curp':'loej1234', 'password':'martinloco'})
        .expect(200)
        .end(function(err, res){
            key = res.body.obj;
            done()
        })
    })
})

describe('Probar lista de burndowns', ()=>{
    it('Deberia de obtener la lista de burndowns', ()=>{
        supertest(app)
        .get('/burndowns/')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    })
    it('Deberia de no obtener la lista de burndownss', ()=>{
        supertest(app)
        .get('/burndowns/')
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})


describe('Probar obtener burndowns por index', ()=>{
    it('Deberia de obtener el burndowns con index', ()=>{
        supertest(app)
        .get('/burndowns/61a9ab67eda8bfad0505092a')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no obtener el burndowns con index', ()=>{
        supertest(app)
        .get('/burndowns/61a9ab67eda8bfad0505092')
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar crear burndowns', ()=>{
    it('Deberia de crear un burndowns nuevo', ()=>{
        supertest(app)
        .post('/burndowns/')
        .send({puntos: 4, tiempo: 10})
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no crear un burndowns nuevo', ()=>{
        supertest(app)
        .post('/burndowns/')
        .send({puntos: 4, tiempo: 10})
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar crear remplazar datos de burndowns', ()=>{
    it('Deberia de modificar burndowns con id', ()=>{
        supertest(app)
        .put('/burndowns/61a9ab67eda8bfad0505092a')
        .send({puntos: 8, tiempo: 20})
        .send({name: 'file 2'})
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no modificar burndowns con id', ()=>{
        supertest(app)
        .put('/burndowns/61a9ab67eda8bfad0505092a')
        .set('Authorization', `Bearer ${key}`)
        .send({puntos: 4, tiempo: 10})
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar editar datos de burndowns', ()=>{
    it('Deberia de modificar burndowns con id', ()=>{
        supertest(app)
        .patch('/burndowns/61a9ab67eda8bfad0505092a')
        .set('Authorization', `Bearer ${key}`)
        .send({puntos: 1, tiempo: 1})
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no modificar burndowns con id', ()=>{
        supertest(app)
        .patch('/burndowns/61a9ab67eda8bfad0505092')
        .set('Authorization', `Bearer ${key}`)
        .send({puntos: 80, tiempo: 80})       
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar eliminar datos de burndowns', ()=>{
    it('Deberia de eliminar burndowns con id', ()=>{
        supertest(app)
        .delete('/burndowns/61a9ab67eda8bfad0505092a')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no eliminar burndowns con id', ()=>{
        supertest(app)
        .delete('/burndowns/61a9ab67eda8bfad0505092')
        .set('Authorization', `Bearer ${key}`)
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})
