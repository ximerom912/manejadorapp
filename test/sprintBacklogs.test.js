const supertest = require('supertest');
const app = require('../app');
var key = ""
//sentencia
describe('Probar el sistema de autentificacion', ()=>{
    //casos de prueba 50%
    it('Deberia de obtener un login con curp y contraseña correcto', (done)=>{
        supertest(app).post('/login')
        .send({'curp':'loej1234', 'password':'martinloco'})
        .expect(200)
        .end(function(err, res){
            key = res.body.obj;
            done()
        })
    })
})

describe('Probar lista de sprintbacklog', ()=>{
    it('Deberia de obtener la lista de sprintbacklog', ()=>{
        supertest(app)
        .get('/sprintBacklogs/')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    })
    it('Deberia de no obtener la lista de sprintbacklogs', ()=>{
        supertest(app)
        .get('/sprintBacklogs/')
        .set('Authorization', `Bearer ${key}`)
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})


describe('Probar obtener sprintbacklog por index', ()=>{
    it('Deberia de obtener el sprintbacklog con index', ()=>{
        supertest(app)
        .get('/sprintBacklogs/61a9a00aeda8bfad05050917')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no obtener el sprintbacklog con index', ()=>{
        supertest(app)
        .get('/sprintBacklogs/61a9a00aeda8bfad0505091')
        .set('Authorization', `Bearer ${key}`)
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar crear sprintbacklog', ()=>{
    it('Deberia de crear un sprintbacklog nuevo', ()=>{
        supertest(app)
        .post('/sprintBacklogs/')
        .send({
            nombre: "sprint 2",
            tiempo: 25,
            historias: ['61a993aeeda8bfad0505090e']
        })
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no crear un sprintbacklog nuevo', ()=>{
        supertest(app)
        .post('/sprintBacklogs/')
        .send({
            nombre: "sprint 5",
            tiempo: 25,
            historias: ['61a993aeeda8bfad0505090e']
        })
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar crear remplazar datos de sprintbacklog', ()=>{
    it('Deberia de modificar sprintbacklog con id', ()=>{
        supertest(app)
        .put('/sprintBacklogs/61a9a00aeda8bfad0505091')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombre: "sprint 1",
            tiempo: 2,
            historias: ['61a993aeeda8bfad0505090e']
        })
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no modificar sprintbacklog con id', ()=>{
        supertest(app)
        .put('/sprintBacklogs/61a9a00aeda8bfad05050')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombre: "sprint 0",
            tiempo: 0,
            historias: ['61a993aeeda8bfad0505090e']
        })
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar editar datos de sprintbacklog', ()=>{
    it('Deberia de modificar sprintbacklog con id', ()=>{
        supertest(app)
        .patch('/sprintBacklogs/61a9a00aeda8bfad050509')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombre: "sprint 101",
            tiempo: 25,
            historias: ['61a993aeeda8bfad0505090e']
        })
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no modificar sprintbacklog con id', ()=>{
        supertest(app)
        .patch('/sprintBacklogs/61a9a00aeda8bfad0505')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombre: "sprint 300",
            tiempo: 25,
            historias: ['61a993aeeda8bfad0505090e']
        })       
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar eliminar datos de sprintbacklog', ()=>{
    it('Deberia de eliminar sprintbacklog con id', ()=>{
        supertest(app)
        .delete('/sprintBacklogs/61a9a00aeda8bfad05050')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no eliminar sprintbacklog con id', ()=>{
        supertest(app)
        .delete('/sprintBacklogs/61a9a00aeda8bfad0505')
        .set('Authorization', `Bearer ${key}`)
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})
