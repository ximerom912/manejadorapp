const supertest = require('supertest');
const app = require('../app');
var key = ""
//sentencia
describe('Probar el sistema de autentificacion', ()=>{
    //casos de prueba 50%
    it('Deberia de obtener un login con curp y contraseña correcto', (done)=>{
        supertest(app).post('/login')
        .send({'curp':'loej1234', 'password':'martinloco'})
        .expect(200)
        .end(function(err, res){
            key = res.body.obj;
            done()
        })
    })
})

describe('Probar lista de burndowns', ()=>{
    it('Deberia de obtener la lista de burndowns', ()=>{
        supertest(app)
        .get('/burndowns/')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    })
    it('Deberia de no obtener la lista de burndownss', ()=>{
        supertest(app)
        .get('/burndowns/')
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})


describe('Probar obtener burndowns por index', ()=>{
    it('Deberia de obtener el burndowns con index', ()=>{
        supertest(app)
        .get('/burndowns/61a9adaeeda8bfad0505092f')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no obtener el burndowns con index', ()=>{
        supertest(app)
        .get('/burndowns/61a9adaeeda8bfad0505092f')
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar crear burndowns', ()=>{
    it('Deberia de crear un burndowns nuevo', ()=>{
        supertest(app)
        .post('/burndowns/')
        .send({
            nombre: "backlog 1",
            productBacklog: '61a9a625eda8bfad05050920',
            sprints: '61a9a00aeda8bfad05050917',
            burnDownChart: '61a9ab67eda8bfad0505092a'
        })
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no crear un burndowns nuevo', ()=>{
        supertest(app)
        .post('/burndowns/')
        .send({
            nombre: "backlog 1",
            productBacklog: '61a9a625eda8bfad05050920',
            sprints: '61a9a00aeda8bfad05050917',
            burnDownChart: '61a9ab67eda8bfad0505092'
        })
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar crear remplazar datos de burndowns', ()=>{
    it('Deberia de modificar burndowns con id', ()=>{
        supertest(app)
        .put('/burndowns/61a9adaeeda8bfad0505092f')
        .send({
            nombre: "backlog 2",
            productBacklog: '61a9a625eda8bfad05050920',
            sprints: '61a9a00aeda8bfad05050917',
            burnDownChart: '61a9ab67eda8bfad0505092a'
        })
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no modificar burndowns con id', ()=>{
        supertest(app)
        .put('/burndowns/61a9adaeeda8bfad0505092f')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombre: "backlog 3",
            productBacklog: '61a9a625eda8bfad05050920',
            sprints: '61a9a00aeda8bfad05050917',
            burnDownChart: '61a9ab67eda8bfad0505092a'
        })
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar editar datos de burndowns', ()=>{
    it('Deberia de modificar burndowns con id', ()=>{
        supertest(app)
        .patch('/burndowns/61a9adaeeda8bfad0505092f')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombre: "backlog 2",
            productBacklog: '61a9a625eda8bfad05050920',
            sprints: '61a9a00aeda8bfad05050917',
            burnDownChart: '61a9ab67eda8bfad0505092a'
        })
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no modificar burndowns con id', ()=>{
        supertest(app)
        .patch('/burndowns/61a9adaeeda8bfad0505092f')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombre: "backlog 5",
            productBacklog: '61a9a625eda8bfad05050920',
            sprints: '61a9a00aeda8bfad05050917',
            burnDownChart: '61a9ab67eda8bfad0505092a'
        })       
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar eliminar datos de burndowns', ()=>{
    it('Deberia de eliminar burndowns con id', ()=>{
        supertest(app)
        .delete('/burndowns/61a9adaeeda8bfad0505092f')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no eliminar burndowns con id', ()=>{
        supertest(app)
        .delete('/burndowns/61a9ab67eda8bfad0505092')
        .set('Authorization', `Bearer ${key}`)
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})
