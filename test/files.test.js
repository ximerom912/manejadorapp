const supertest = require('supertest');
const app = require('../app');
var key = ""
//sentencia
describe('Probar el sistema de autentificacion', ()=>{
    //casos de prueba 50%
    it('Deberia de obtener un login con curp y contraseña correcto', (done)=>{
        supertest(app).post('/login')
        .send({'curp':'loej1234', 'password':'martinloco'})
        .expect(200)
        .end(function(err, res){
            key = res.body.obj;
            done()
        })
    })
})

describe('Probar lista de files', ()=>{
    it('Deberia de obtener la lista de files', ()=>{
        supertest(app)
        .get('/files/')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    })
    it('Deberia de no obtener la lista de filess', ()=>{
        supertest(app)
        .get('/files/')
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})


describe('Probar obtener files por index', ()=>{
    it('Deberia de obtener el files con index', ()=>{
        supertest(app)
        .get('/files/61a9a98eeda8bfad05050925')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no obtener el files con index', ()=>{
        supertest(app)
        .get('/files/61a9a98eeda8bfad05050925')
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar crear files', ()=>{
    it('Deberia de crear un files nuevo', ()=>{
        supertest(app)
        .post('/files/61a9a98eeda8bfad05050925')
        .send({
            nombreProyecto: "App productividad",
            fechaSolicitudProyecto: '10-05-14',
            fechaArranqueProyecto: '20-05-18',
            descripcionProyecto: "Proyecto complicadito",
            scrumMaster: '61a9719a28033bd1099e20bf'
        })
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no crear un files nuevo', ()=>{
        supertest(app)
        .post('/files/61a9a98eeda8bfad05050925')
        .send({
            nombreProyecto: "App para pasar tareas",
            fechaSolicitudProyecto: '10-05-14',
            fechaArranqueProyecto: '20-05-18',
            descripcionProyecto: "Proyecto facilito",
            scrumMaster: '61a9719a28033bd1099e20bf'
        })
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar crear remplazar datos de files', ()=>{
    it('Deberia de modificar files con id', ()=>{
        supertest(app)
        .put('/files/61a9a98eeda8bfad05050925')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombreProyecto: "App uber",
            fechaSolicitudProyecto: '10-07-17',
            fechaArranqueProyecto: '20-05-18',
            descripcionProyecto: "Proyecto moderado",
            scrumMaster: '61a9719a28033bd1099e20bf'
        })
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no modificar files con id', ()=>{
        supertest(app)
        .put('/files/61a9a98eeda8bfad0505092')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombreProyecto: "App productividad a menos",
            fechaSolicitudProyecto: '10-05-14',
            fechaArranqueProyecto: '20-05-18',
            descripcionProyecto: "Proyecto",
            scrumMaster: ''
        })
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar editar datos de files', ()=>{
    it('Deberia de modificar files con id', ()=>{
        supertest(app)
        .patch('/files/61a9a98eeda8bfad05050925')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombreProyecto: "App productividad pro",
            fechaSolicitudProyecto: '10-05-14',
            fechaArranqueProyecto: '20-05-18',
            descripcionProyecto: "Proyecto complicadito y de paga",
            scrumMaster: '61a9719a28033bd1099e20bf'
        })
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no modificar files con id', ()=>{
        supertest(app)
        .patch('/files/61a9a98eeda8bfad0505092')
        .set('Authorization', `Bearer ${key}`)
        .send({
            nombreProyecto: "App sueno",
            fechaSolicitudProyecto: '10-05-14',
            fechaArranqueProyecto: '20-05-18',
            descripcionProyecto: "Proyecto facilito",
            scrumMaster: '61a9719a28033bd1099e20bf'
        })        
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})

describe('Probar eliminar datos de files', ()=>{
    it('Deberia de eliminar files con id', ()=>{
        supertest(app)
        .delete('/files/61a9a98eeda8bfad05050925')
        .set('Authorization', `Bearer ${key}`)
        .expect(200)
        .end(function(err,res){
            done()
        })
    }),
    it('Deberia de no eliminar files con id', ()=>{
        supertest(app)
        .delete('/files/61a9a98eeda8bfad05050925')
        .set('Authorization', `Bearer ${key}`)
        .expect(500)
        .end(function(err,res){
            done()
        })
    })
})
