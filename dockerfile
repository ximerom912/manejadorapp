FROM node
MAINTAINER Martin Loera, Melissa Garcia, Ximena Romero
WORKDIR /app
COPY . .
RUN npm install
EXPOSE 3000
CMD npm start 