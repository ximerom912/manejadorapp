const { json } = require('express');
const express = require('express');
const Member = require('../models/member');

//RESFULL = GET, POST, PUT, PACH Y DELETE
//Modelo = Una estructura de datos que representa una entidad real
function list(req, res, next){
    Member.find().then(objs => res.status(200).json({
        message: res.__('ok.list'),
        obj: objs
    })).catch(ex => res.status(500).json({
        message: res.__('bad.list'),
        obj:ex
    }));
}
 
function index(req, res, next){
    const id = req.params.id;
    Member.findOne({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.index'),
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.index'),
        obj: ex
    }));
}

function create(req, res, next){
    const listaHabilidades= req.body.listaHabilidades;
    const rol= req.body.rol;

    let member = new Member({
        listaHabilidades:listaHabilidades,
        rol:rol
    });

    member.save().then(obj => res.status(200).json({
        message: res.__('ok.create'),
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.create'),
        obj:ex
    }));
}

function replace(req, res, next){
    const id = req.params.id;
    const listaHabilidades= req.body.listaHabilidades ? req.body.listaHabilidades: "";
    const rol= req.body.rol ? req.body.rol: "";

    let member = new Object({
        _listaHabilidades:listaHabilidades,
        _rol:rol
    });

    Member.findOneAndUpdate({"_id":id},member).then(obj => res.status(200).json({
        message: res.__('ok.replace'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.replace'),
        obj:ex
    }));
}

function edit(req, res, next){
    const id = req.params.id;
    const listaHabilidades= req.body.listaHabilidades;
    const rol= req.body.rol;


    let member = new Object();

    if(listaHabilidades){
        member._listaHabilidades = listaHabilidades;
    }

    if(rol){
        member._rol = rol;
    }

    Member.findOneAndUpdate({"_id":id},member).then(obj => res.status(200).json({
        message:res.__('ok.edit'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.edit'),
        obj:ex
    }));
    
}

function destroy(req, res, next){
    const id = req.params.id;
    
    Member.remove({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.destroy'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.destroy'),
        obj:ex
    }));
}

module.exports = {
    list, index, create, replace, edit, destroy
}