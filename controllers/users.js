const express = require('express');
const bcrypt = require('bcrypt');
const async = require('async');
const config = require('config');
const User = require('../models/user')

//RESFULL = GET, POST, PUT, PACH Y DELETE
//Modelo = Una estructura de datos que representa una entidad real
function list(req, res, next){
    let page = req.params.page ? req.params.page : 1;
    User.paginate({},{page:page,limit:config.get("paginate.size")}).then(objs => res.status(200).json({
        message: res.__('ok.list'),
        obj: objs
    })).catch(ex => res.status(500).json({
        message: res.__('bad.list'),
        obj:ex
    }));
}
 
function index(req, res, next){
    const id = req.params.id;
    User.findOne({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.index'),
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.index'),
        obj: ex
    }));
}

function create(req, res, next){
    const nombre= req.body.nombre;
    const fecha= req.body.fecha;
    const curp= req.body.curp;
    const rfc= req.body.rfc;
    const domicilio= req.body.domicilio;
    const password = req.body.password;

    async.parallel({
        salt:(callback)=>{
            bcrypt.genSalt(10, callback);
        }
    }, (err, result)=>{
        bcrypt.hash(password, result.salt, (err, hash)=>{
            let user = new User({
                nombre:nombre,
                fecha:fecha,
                curp:curp,
                rfc:rfc,
                domicilio:domicilio,
                password:hash,
                salt:result.salt
            });
        
            user.save().then(obj => res.status(200).json({
                message: res.__('ok.create'),
                obj: obj
            })).catch(err => res.status(500).json({
                message: res.__('bad.create'),
                obj:err
            }));
        });
        });
}
    

function replace(req, res, next){
    const id = req.params.id;
    const nombre= req.body.nombre ? req.body.nombre: "";
    const fecha= req.body.fecha ? req.body.fecha: "";
    const curp= req.body.curp ? req.body.curp: "";
    const rfc= req.body.rfc ? req.body.rfc: "";
    const domicilio= req.body.domicilio ? req.body.domicilio: "";
    const password = req.body.password ? req.body.password: "";


    let user = new Object({
        _nombre:nombre,
        _fecha:fecha,
        _curp:curp,
        _rfc:rfc,
        _domicilio:domicilio,
        _password:password
    });    

    User.findOneAndUpdate({"_id":id},user).then(obj => res.start(200).json({
        message: res.__('ok.replace'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.replace'),
        obj:ex
    }));
}

function edit(req, res, next){
    const id = req.params.id;
    const nombre= req.body.nombre;
    const fecha= req.body.fecha;
    const curp= req.body.curp;
    const rfc= req.body.rfc;
    const domicilio= req.body.domicilio;
    const password = req.body.password;

    let user = new Object();

    if(nombre){
        user._nombre = nombre;
    }

    if(fecha){
        user._fecha = fecha;
    }

    if(curp){
        user._curp = curp;
    }

    if(rfc){
        user._rfc = rfc;
    }

    if(domicilio){
        user._domicilio = domicilio;
    }

    if(password){
        user._password = password;
    }

    User.findOneAndUpdate({"_id":id},user).then(obj => res.start(200).json({
        message: res.__('ok.edit'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.edit'),
        obj:ex
    }));
}

function destroy(req, res, next){
    const id = req.params.id;
    
    User.remove({"_id":id}).then(obj => res.start(200).json({
        message: res.__('ok.destroy'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.destroy'),
        obj:ex
    }));
}

module.exports = {
    list, index, create, replace, edit, destroy
}
