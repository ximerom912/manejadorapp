const { json } = require('express');
const express = require('express');
const UserHistory = require('../models/userHistory');

//RESFULL = GET, POST, PUT, PACH Y DELETE
//Modelo = Una estructura de datos que representa una entidad real
function list(req, res, next){
    UserHistory.find().then(objs => res.status(200).json({
        message: res.__('ok.list'),
        obj: objs
    })).catch(ex => res.status(500).json({
        message: res.__('bad.list'),
        obj:ex
    }));
}
 
function index(req, res, next){
    const id = req.params.id;
    UserHistory.findOne({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.index'),
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.index'),
        obj: ex
    }));
}

function create(req, res, next){
    const nombre= req.body.nombre;
    const rol= req.body.rol;
    const funcionalidad= req.body.funcionalidad;
    const beneficio= req.body.beneficio;
    const prioridad= req.body.prioridad;
    const tamano= req.body.tamano;
    const resultado= req.body.resultado;
    const terminado= req.body.terminado;
    const puntos= req.body.puntos;
    const aceptado= req.body.aceptado;

    let userHistory = new UserHistory({
        nombre:nombre,
        rol:rol,
        funcionalidad:funcionalidad,
        beneficio:beneficio,
        prioridad:prioridad,
        tamano:tamano,
        resultado:resultado,
        terminado: terminado,
        puntos:puntos,
        aceptado:aceptado
    });

    userHistory.save().then(obj => res.status(200).json({
        message: res.__('ok.create'),
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.create'),
        obj:ex
    }));
}

function replace(req, res, next){
    const id = req.params.id;
    const nombre= req.body.nombre ? req.body.nombre: "";
    const rol= req.body.rol ? req.body.rol: "";
    const funcionalidad= req.body.funcionalidad ? req.body.funcionalidad: "";
    const beneficio= req.body.beneficio ? req.body.beneficio: "";
    const prioridad= req.body.prioridad ? req.body.prioridad: "";
    const tamano= req.body.tamano ? req.body.tamano: "";
    const resultado= req.body.resultado ? req.body.resultado: "";
    const terminado= req.body.terminado ? req.body.terminado: "";
    const puntos= req.body.puntos ? req.body.puntos: "";
    const aceptado= req.body.aceptado ? req.body.aceptado: "";

    let userHistory = new Object({
        _nombre:nombre,
        _rol:rol,
        _funcionalidad:funcionalidad,
        _beneficio:beneficio,
        _prioridad:prioridad,
        _tamano:tamano,
        _resultado:resultado,
        _terminado: terminado,
        _puntos:puntos,
        _aceptado:aceptado
    });

    UserHistory.findOneAndUpdate({"_id":id},userHistory).then(obj => res.status(200).json({
        message: res.__('ok.replace'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.replace'),
        obj:ex
    }));
}

function edit(req, res, next){
    const id = req.params.id;
    const nombre= req.body.nombre;
    const rol= req.body.rol;
    const funcionalidad= req.body.funcionalidad;
    const beneficio= req.body.beneficio;
    const prioridad= req.body.prioridad;
    const tamano= req.body.tamano;
    const resultado= req.body.resultado;
    const terminado= req.body.terminado;
    const puntos= req.body.puntos;
    const aceptado= req.body.aceptado;

    let userHistory = new Object();

    if(nombre){
        userHistory._nombre = nombre;
    }

    if(rol){
        userHistory._rol = rol;
    }

    if(funcionalidad){
        userHistory._funcionalidad = funcionalidad;
    }

    if(beneficio){
        userHistory._beneficio = beneficio;
    }

    if(prioridad){
        userHistory._prioridad = prioridad;
    }

    if(tamano){
        userHistory._tamano = tamano;
    }

    if(resultado){
        userHistory._resultado = resultado;
    }

    if(terminado){
        userHistory._terminado = terminado;
    }

    if(puntos){
        userHistory._puntos = puntos;
    }

    if(aceptado){
        userHistory._aceptado = aceptado;
    }
    UserHistory.findOneAndUpdate({"_id":id},userHistory).then(obj => res.status(200).json({
        message:res.__('ok.edit'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.edit'),
        obj:ex
    }));
    
}

function destroy(req, res, next){
    const id = req.params.id;
    
    UserHistory.remove({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.destroy'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.destroy'),
        obj:ex
    }));
}

module.exports = {
    list, index, create, replace, edit, destroy
}