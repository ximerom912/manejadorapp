const { json } = require('express');
const express = require('express');
const Burndown = require('../models/burndown');

//RESFULL = GET, POST, PUT, PACH Y DELETE
//Modelo = Una estructura de datos que representa una entidad real
function list(req, res, next){
    Burndown.find().then(objs => res.status(200).json({
        message: res.__('ok.list'),
        obj: objs
    })).catch(ex => res.status(500).json({
        message: res.__('bad.list'),
        obj:ex
    }));
}
 
function index(req, res, next){
    const id = req.params.id;
    Burndown.findOne({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.index'),
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.index'),
        obj: ex
    }));
}

function create(req, res, next){
    const puntos= req.body.puntos;
    const tiempo= req.body.tiempo;

    let burndown = new Burndown({
        puntos:puntos,
        tiempo:tiempo
    });

    burndown.save().then(obj => res.status(200).json({
        message: res.__('ok.create'),
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.create'),
        obj:ex
    }));
}

function replace(req, res, next){
    const id = req.params.id;
    const puntos= req.body.puntos ? req.body.puntos: "";
    const tiempo= req.body.tiempo ? req.body.tiempo: "";

    let burndown = new Object({
        _puntos:puntos,
        _tiempo:tiempo
    });

    Burndown.findOneAndUpdate({"_id":id},burndown).then(obj => res.status(200).json({
        message: res.__('ok.replace'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.replace'),
        obj:ex
    }));
}

function edit(req, res, next){
    const id = req.params.id;
    const puntos= req.body.puntos;
    const tiempo= req.body.tiempo;

    let burndown = new Object();

    if(puntos){
        burndown._puntos = puntos;
    }

    if(tiempo){
        burndown._tiempo = tiempo;
    }

    Burndown.findOneAndUpdate({"_id":id},burndown).then(obj => res.status(200).json({
        message:res.__('ok.edit'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.edit'),
        obj:ex
    }));
    
}

function destroy(req, res, next){
    const id = req.params.id;
    
    Burndown.remove({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.destroy'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.destroy'),
        obj:ex
    }));
}

module.exports = {
    list, index, create, replace, edit, destroy
}