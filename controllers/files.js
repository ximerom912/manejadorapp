const { json } = require('express');
const express = require('express');
const File = require('../models/file');

//RESFULL = GET, POST, PUT, PACH Y DELETE
//Modelo = Una estructura de datos que representa una entidad real
function list(req, res, next){
    File.find().then(objs => res.status(200).json({
        message: res.__('ok.list'),
        obj: objs
    })).catch(ex => res.status(500).json({
        message: res.__('bad.list'),
        obj:ex
    }));
}
 
function index(req, res, next){
    const id = req.params.id;
    File.findOne({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.index'),
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.index'),
        obj: ex
    }));
}

function create(req, res, next){
    const nombreProyecto= req.body.nombreProyecto;
    const fechaSolicitudProyecto= req.body.fechaSolicitudProyecto;
    const fechaArranqueProyecto= req.body.fechaArranqueProyecto;
    const descripcionProyecto= req.body.descripcionProyecto;
    const scrumMaster= req.body.scrumMaster;
    const productOwner= req.body.productOwner;
    const equipoDesarrollo= req.body.equipoDesarrollo;
    const backlog= req.body.backlog;

    let file = new File({
        nombreProyecto:nombreProyecto,
        fechaSolicitudProyecto:fechaSolicitudProyecto,
        fechaArranqueProyecto:fechaArranqueProyecto,
        descripcionProyecto:descripcionProyecto,
        scrumMaster:scrumMaster,
        productOwner:productOwner,
        equipoDesarrollo:equipoDesarrollo,
        backlog:backlog
    });

    file.save().then(obj => res.status(200).json({
        message: res.__('ok.create'),
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.create'),
        obj:ex
    }));
}

function replace(req, res, next){
    const id = req.params.id;
    const nombreProyecto= req.body.nombreProyecto ? req.body.nombreProyecto: "";
    const fechaSolicitudProyecto= req.body.fechaSolicitudProyecto ? req.body.fechaSolicitudProyecto: "";
    const fechaArranqueProyecto= req.body.fechaArranqueProyecto ? req.body.fechaArranqueProyecto: "";
    const descripcionProyecto= req.body.descripcionProyecto ? req.body.descripcionProyecto: "";
    const scrumMaster= req.body.scrumMaster ? req.body.scrumMaster: "";
    const productOwner= req.body.productOwner ? req.body.productOwner: "";
    const equipoDesarrollo= req.body.equipoDesarrollo ? req.body.equipoDesarrollo: "";
    const backlog= req.body.backlog ? req.body.backlog: "";

    let file = new Object({
        _nombreProyecto:nombreProyecto,
        _fechaSolicitudProyecto:fechaSolicitudProyecto,
        _fechaArranqueProyecto:fechaArranqueProyecto,
        _descripcionProyecto:descripcionProyecto,
        _scrumMaster:scrumMaster,
        _productOwner:productOwner,
        _equipoDesarrollo:equipoDesarrollo,
        _backlog:backlog
    });

    File.findOneAndUpdate({"_id":id},file).then(obj => res.status(200).json({
        message: res.__('ok.replace'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.replace'),
        obj:ex
    }));
}

function edit(req, res, next){
    const id = req.params.id;
    const nombreProyecto= req.body.nombreProyecto;
    const fechaSolicitudProyecto= req.body.fechaSolicitudProyecto;
    const fechaArranqueProyecto= req.body.fechaArranqueProyecto;
    const descripcionProyecto= req.body.descripcionProyecto;
    const scrumMaster= req.body.scrumMaster;
    const productOwner= req.body.productOwner;
    const equipoDesarrollo= req.body.equipoDesarrollo;
    const backlog= req.body.backlog;

    let file = new Object();

    if(nombreProyecto){
        file._nombreProyecto = nombreProyecto;
    }

    if(fechaSolicitudProyecto){
        file._fechaSolicitudProyecto = fechaSolicitudProyecto;
    }

    if(fechaArranqueProyecto){
        file._fechaArranqueProyecto = fechaArranqueProyecto;
    }

    if(descripcionProyecto){
        file._descripcionProyecto = descripcionProyecto;
    }

    if(scrumMaster){
        file._scrumMaster = scrumMaster;
    }

    if(productOwner){
        file._productOwner = productOwner;
    }

    if(equipoDesarrollo){
        file._equipoDesarrollo = equipoDesarrollo;
    }

    if(backlog){
        file._backlog = backlog;
    }

    File.findOneAndUpdate({"_id":id},file).then(obj => res.status(200).json({
        message:res.__('ok.edit'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.edit'),
        obj:ex
    }));
    
}

function destroy(req, res, next){
    const id = req.params.id;
    
    File.remove({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.destroy'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.destroy'),
        obj:ex
    }));
}

module.exports = {
    list, index, create, replace, edit, destroy
}