const { json } = require('express');
const express = require('express');
const Backlog = require('../models/backlog');

//RESFULL = GET, POST, PUT, PACH Y DELETE
//Modelo = Una estructura de datos que representa una entidad real
function list(req, res, next){
    Backlog.find().then(objs => res.status(200).json({
        message: res.__('ok.list'),
        obj: objs
    })).catch(ex => res.status(500).json({
        message: res._('bad.list'),
        obj:ex
    }));
}
 
function index(req, res, next){
    const id = req.params.id;
    Backlog.findOne({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.index'),
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.index'),
        obj: ex
    }));
}

function create(req, res, next){
    const nombre = req.body.nombre;
    const productBacklog = req.body.productBacklog;
    const sprints  = req.body.sprints;
    const burdDownChart  = req.body.burdDownChart;

    let backlog = new Backlog({
        nombre:nombre,
        productBacklog:productBacklog,
        sprints:sprints,
        burdDownChart:burdDownChart
    });

    backlog.save().then(obj => res.status(200).json({
        message: res.__('ok.create'),
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.create'),
        obj:ex
    }));
}

function replace(req, res, next){
    const id = req.params.id;
    const nombre = req.body.nombre ? req.body.nombre: "";
    const productBacklog = req.body.productBacklog ? req.body.productBacklog: "";
    const sprints  = req.body.sprints ? req.body.sprints: "";
    const burdDownChart  = req.body.burdDownChart ? req.body.burdDownChart: "";

    let backlog = new Object({
        _nombre:nombre,
        _productBacklog:productBacklog,
        _sprints:sprints,
        _burdDownChart:burdDownChart
    });

    Backlog.findOneAndUpdate({"_id":id},backlog).then(obj => res.status(200).json({
        message: res.__('ok.replace'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.replace'),
        obj:ex
    }));
}

function edit(req, res, next){
    const id = req.params.id;
    const nombre = req.body.nombre;
    const productBacklog = req.body.productBacklog;
    const sprints  = req.body.sprints;
    const burdDownChart  = req.body.burdDownChart;

    let backlog = new Object();

    if(nombre){
        backlog._nombre = nombre;
    }

    if(productBacklog){
        backlog._productBacklog = productBacklog;
    }

    if(sprints){
        backlog._sprints = sprints;
    }

    if(burdDownChart){
        backlog._burdDownChart = burdDownChart;
    }

    Backlog.findOneAndUpdate({"_id":id},backlog).then(obj => res.status(200).json({
        message:res.__('ok.edit'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.edit'),
        obj:ex
    }));
    
}

function destroy(req, res, next){
    const id = req.params.id;
    
    Backlog.remove({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.destroy'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.destroy'),
        obj:ex
    }));
}

module.exports = {
    list, index, create, replace, edit, destroy
}