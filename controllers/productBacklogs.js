const { json } = require('express');
const express = require('express');
const ProductBacklog = require('../models/productBacklog');

//RESFULL = GET, POST, PUT, PACH Y DELETE
//Modelo = Una estructura de datos que representa una entidad real
function list(req, res, next){
    ProductBacklog.find().then(objs => res.status(200).json({
        message: res.__('ok.list'),
        obj: objs
    })).catch(ex => res.status(500).json({
        message: res.__('bad.list'),
        obj:ex
    }));
}
 
function index(req, res, next){
    const id = req.params.id;
    ProductBacklog.findOne({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.index'),
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.index'),
        obj: ex
    }));
}

function create(req, res, next){
    const nombre= req.body.nombre;
    const tiempo= req.body.tiempo;
    const historias= req.body.historias;

    let productBacklog = new ProductBacklog({
        nombre:nombre,
        tiempo:tiempo,
        historias:historias
    });

    productBacklog.save().then(obj => res.status(200).json({
        message: res.__('ok.create'),
        obj: obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.create'),
        obj:ex
    }));
}

function replace(req, res, next){
    const id = req.params.id;
    const nombre= req.body.nombre ? req.body.nombre: "";
    const tiempo= req.body.tiempo ? req.body.tiempo: "";
    const historias= req.body.historias ? req.body.historias: "";

    let productBacklog = new Object({
        _nombre:nombre,
        _tiempo:tiempo,
        _historias:historias
    });

    ProductBacklog.findOneAndUpdate({"_id":id},productBacklog).then(obj => res.status(200).json({
        message: res.__('ok.replace'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.replace'),
        obj:ex
    }));
}

function edit(req, res, next){
    const id = req.params.id;
    const nombre= req.body.nombre;
    const tiempo= req.body.tiempo;
    const historias= req.body.historias;

    let productBacklog = new Object();

    if(nombre){
        productBacklog._nombre = nombre;
    }

    if(tiempo){
        productBacklog._tiempo = tiempo;
    }

    ProductBacklog.findOneAndUpdate({"_id":id},productBacklog).then(obj => res.status(200).json({
        message:res.__('ok.edit'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.edit'),
        obj:ex
    }));
    
}

function destroy(req, res, next){
    const id = req.params.id;
    
    ProductBacklog.remove({"_id":id}).then(obj => res.status(200).json({
        message: res.__('ok.destroy'),
        obj:obj
    })).catch(ex => res.status(500).json({
        message: res.__('bad.destroy'),
        obj:ex
    }));
}

module.exports = {
    list, index, create, replace, edit, destroy
}