const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
var Schema = mongoose.Schema;

const schema = mongoose.Schema({
    _nombre:String,
    _tiempo:Number,
    _historias:Array
});

class ProductBacklog {
    constructor(nombre,tiempo,historias){
        this.nombre = nombre;
        this.tiempo = tiempo;
        this.historias = historias;
    }

    get nombre(){
        return this._nombre;
    }

    set nombre(v){
        this._nombre = v;
    }
    
    get tiempo(){
        return this._tiempo;
    }

    set tiempo(v){
        this._tiempo = v;
    }

    get historias(){
        return this._historias;
    }

    set historias(v){
        this._historias = v;
    }
}

schema.loadClass(ProductBacklog);
schema.plugin(mongoosePaginate);
module.exports = mongoose.model('ProductBacklog',schema);