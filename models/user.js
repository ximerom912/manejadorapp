const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
var Schema = mongoose.Schema;

const schema = mongoose.Schema({
    _nombre:String,
    _fecha:Date,
    _curp:String,
    _rfc:String,
    _domicilio:String,
    _password:String,
    _salt:String
});

class User {
    constructor(nombre,fecha,curp,rfc,domicilio,password){
        this.nombre = nombre;
        this.fecha = fecha;
        this.curp = curp;
        this.rfc = rfc;
        this.domicilio = domicilio;
        this.password = password;
        this._salt=salt;
    }

    get nombre(){
        return this._nombre;
    }

    set nombre(v){
        this._nombre = v;
    }
    
    get fecha(){
        return this._fecha;
    }

    set fecha(v){
        this._fecha = v;
    }

    get curp(){
        return this._curp;
    }

    set curp(v){
        this._curp = v;
    }
    
    get rfc(){
        return this._rfc;
    }

    set rfc(v){
        this._rfc = v;
    }

    get domicilio(){
        return this._domicilio;
    }

    set domicilio(v){
        this._domicilio = v;
    }

    get password(){
        return this._password;
    }

    set password(v){
        this._password = v;
    }

    get salt(){
        return this._salt;
    }

    set salt(v){
        this._salt = v;
    }
}

schema.loadClass(User);
schema.plugin(mongoosePaginate);
module.exports = mongoose.model('User',schema);