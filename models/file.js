const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
var Schema = mongoose.Schema;

const schema = mongoose.Schema({
    _nombreProyecto:String,
    _fechaSolicitudProyecto:Date,
    _fechaArranqueProyecto:Date,
    _descripcionProyecto:String,
    _scrumMaster: {type: Schema.ObjectId, ref: "scrumMaster" },
    _productOwner:{type: Schema.ObjectId, ref: "productOwner" },
    _equipoDesarrollo:Array,
    _backlog:{type: Schema.ObjectId, ref: "backlog" }
});

class File {
    constructor(nombreProyecto,fechaSolicitudProyecto,fechaArranqueProyecto,descripcionProyecto,scrumMaster,productOwner,equipoDesarrollo,backlog){
        this.nombreProyecto = nombreProyecto;
        this.fechaArranqueProyecto = fechaArranqueProyecto;
        this.fechaSolicitudProyecto = fechaSolicitudProyecto;
        this.descripcionProyecto = descripcionProyecto;
        this.scrumMaster = scrumMaster;
        this.productOwner = productOwner;
        this.equipoDesarrollo = equipoDesarrollo;
        this.backlog = backlog;
    }

    get nombreProyecto(){
        return this._nombreProyecto;
    }

    set nombreProyecto(v){
        this._nombreProyecto = v;
    }
    
    get fechaSolicitudProyecto(){
        return this._fechaSolicitudProyecto;
    }

    set fechaSolicitudProyecto(v){
        this._fechaSolicitudProyecto = v;
    }

    get descripcionProyecto(){
        return this._descripcionProyecto;
    }

    set descripcionProyecto(v){
        this._descripcionProyecto = v;
    }
    
    get scrumMaster(){
        return this._scrumMaster;
    }

    set scrumMaster(v){
        this._scrumMaster = v;
    }

    get productOwner(){
        return this._productOwner;
    }

    set productOwner(v){
        this._productOwner = v;
    }
    
    get equipoDesarrollo(){
        return this._equipoDesarrollo;
    }

    set equipoDesarrollo(v){
        this._equipoDesarrollo = v;
    }

    get backlog(){
        return this._backlog;
    }

    set backlog(v){
        this._backlog = v;
    }
}

schema.loadClass(File);
schema.plugin(mongoosePaginate);
module.exports = mongoose.model('File',schema);