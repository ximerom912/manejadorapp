const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
var Schema = mongoose.Schema;

const schema = mongoose.Schema({
    _nombre:String,
    _rol:String,
    _funcionalidad:String,
    _beneficio:String,
    _prioridad: String,
    _tamano:Number,
    _resultado:String,
    _terminado:Boolean,
    _puntos:Number,
    _aceptado:Boolean
});

class UserHistory {
    constructor(nombre,rol,funcionalidad,beneficio,prioridad,tamano,resultado,terminado,puntos,aceptado){
        this.nombre = nombre;
        this.rol = rol;
        this.funcionalidad = funcionalidad;
        this.beneficio = beneficio;
        this.prioridad = prioridad;
        this.tamano = tamano;
        this.resultado = resultado;
        this.terminado = terminado;
        this.puntos = puntos;
        this.aceptado = aceptado;
    }

    get nombre(){
        return this._nombre;
    }

    set nombre(v){
        this._nombre = v;
    }
    
    get rol(){
        return this._rol;
    }

    set rol(v){
        this._rol = v;
    }

    get funcionalidad(){
        return this._funcionalidad;
    }

    set funcionalidad(v){
        this._funcionalidad = v;
    }
    
    get beneficio(){
        return this._beneficio;
    }

    set beneficio(v){
        this._beneficio = v;
    }

    get prioridad(){
        return this._prioridad;
    }

    set prioridad(v){
        this._prioridad = v;
    }
    
    get tamano(){
        return this._tamano;
    }

    set tamano(v){
        this._tamano = v;
    }

    get resultado(){
        return this._resultado;
    }

    set resultado(v){
        this._resultado = v;
    }

    get puntos(){
        return this._puntos;
    }

    set puntos(v){
        this._puntos = v;
    }

    get aceptado(){
        return this._aceptado;
    }

    set aceptado(v){
        this._aceptado = v;
    }
}

schema.loadClass(UserHistory);
schema.plugin(mongoosePaginate);
module.exports = mongoose.model('UserHistory',schema);