const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
var Schema = mongoose.Schema;

const schema = mongoose.Schema({
    _listaHabilidades: String,
    _rol:String
});

class Member {
    constructor(listaHabilidades,rol){
        this.listaHabilidades = listaHabilidades;
        this.rol = rol;
    }

    get listaHabilidades(){
        return this._listaHabilidades;
    }

    set listaHabilidades(v){
        this._listaHabilidades = v;
    }
    
    get rol(){
        return this._rol;
    }

    set rol(v){
        this._rol = v;
    }
}

schema.loadClass(Member);
schema.plugin(mongoosePaginate);
module.exports = mongoose.model('Member',schema);