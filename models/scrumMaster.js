const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
var Schema = mongoose.Schema;

const schema = mongoose.Schema({
    _rol:String,
});

class ScrumMaster {
    constructor(rol){
        this.rol = rol;
    }
    get rol(){
        return this._rol;
    }

    set rol(v){
        this._rol = v;
    }
}

schema.loadClass(ScrumMaster);
schema.plugin(mongoosePaginate);
module.exports = mongoose.model('ScrumMaster',schema);