const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
var Schema = mongoose.Schema;

const schema = mongoose.Schema({
    _nombre:String,
    _productBacklog:{type: Schema.ObjectId, ref: "productBacklog" },
    _sprints:Array,
    _burdDownChart:{type: Schema.ObjectId, ref: "burndown" }
});

class Backlog {
    constructor(nombre,productBacklog,sprints,burdDownChart){
        this.nombre = nombre;
        this.productBacklog = productBacklog;
        this.sprints = sprints;
        this.burdDownChart = burdDownChart;
    }

    get nombre(){
        return this._nombre;
    }

    set nombre(v){
        this._nombre = v;
    }
    
    get productBacklog(){
        return this._productBacklog;
    }

    set productBacklog(v){
        this._productBacklog = v;
    }

    get sprints(){
        return this._sprints;
    }

    set sprints(v){
        this._sprints = v;
    }

    get burdDownChart(){
        return this._burdDownChart;
    }

    set burdDownChart(v){
        this._burdDownChart = v;
    }
}

schema.loadClass(Backlog);
schema.plugin(mongoosePaginate);
module.exports = mongoose.model('Backlog',schema);