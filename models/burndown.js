const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');
var Schema = mongoose.Schema;

const schema = mongoose.Schema({
    _puntos:Number,
    _tiempo:Number
});

class Burndown {
    constructor(puntos,tiempo){
        this.puntos = puntos;
        this.tiempo = tiempo;
    }

    get puntos(){
        return this._puntos;
    }

    set puntos(v){
        this._puntos = v;
    }
    
    get tiempo(){
        return this._tiempo;
    }

    set tiempo(v){
        this._tiempo = v;
    }
}

schema.loadClass(Burndown);
schema.plugin(mongoosePaginate);
module.exports = mongoose.model('Burndown',schema);